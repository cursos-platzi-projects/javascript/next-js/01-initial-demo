import { useRouter } from 'next/router';

import Link from 'next/link';

//Este estilo se pone fuera del componente para que no se renderice de nuevo, por performance
const style = {
    color: '#0070f3',
    textDecoration: 'underline'
}

export const ActiveLink = ({ text, href }) => {

    //useRouter = obtiene la informacion de la pagina, en este caso obtenemos la ruta de esta pagina
    const { asPath } = useRouter();

    return (
        <Link href={ href }>
            <a style={ asPath === href ? style : null }>{ text }</a>
        </Link>
        );
};



